(ns medularis-chalensh.routes
  (:use [compojure.core :only [defroutes GET POST]]
        [ring.middleware.json :only [wrap-json-params wrap-json-response]]
        [ring.util.response :only [response status not-found]]
        [ring.middleware.params :only [wrap-params]]
        [ring.middleware.keyword-params :only [wrap-keyword-params]]
        [environ.core :only [env]])
  (:require [ring.util.response :as response]
            [honeysql.core :as hql]
            [medularis-chalensh.models :as model]
            [clj-http.client :as client]
            [cheshire.core :as json]
            [taoensso.timbre :as log]))

(def twilio (str "https://api.twilio.com/2010-04-01/Accounts/"
                 (:twilio-sid env)
                 "/Messages.json"))

(defn wrap-exception
  "Custom Ring middleware. Returns an Internal Server Error response
  when encountering uncaught exceptions"
  [f]
  (fn [request]
    (try (f request)
         (catch Exception e
           (log/error e)
           (-> (response {:message "Internal server error"})
               (status 500))))))

(defn post-twilio
  "Actually performs the POST call to Twiio's SMS API"
  [message]
  (client/post twilio {:basic-auth [(:twilio-sid env)
                                    (:auth-token env)]
                       :form-params {:Format "json"
                                     :AccountSid (:twilio-sid env)
                                     :From (:twilio-from env)
                                     :To (:twilio-to env)
                                     :Body message}
                       :as :json}))

(defn send-message
  "Controller logic for sending messages.

  Mostly deals in deciding the proper HTTP responses"
  [message]
  (let [result (post-twilio message)
        standard-fields (select-keys (:body result)
                                     [:sid :to :from :body :status])
        error-fields (select-keys (:body result)
                                  [:message])]
    (case (:status result)
      201 (let [[row] (model/save-response message
                                           (:status result)
                                           (:body result))]
            (log/info row)
            (-> (response {:twilio-response standard-fields :id (:id row)})
                (status 201)))

      401 (-> (response {:message "Internal server error"})
              (status 500))
      ;; else
      (-> (response error-fields)
          (status (:status result))))))

(defn get-message
  "Retrieves a message record from the DB, as-is"
  [id]
  (if-let [row (model/get-message id)]
    (response  row)
    (not-found {:message  "Not found"})))

(defroutes sms
  (POST "/api/sms" [message]
        (do (log/info message)
            (send-message message)))
  (GET "/api/sms/:id" [id]
       (try
         (get-message (Integer/parseInt id))
         (catch java.lang.NumberFormatException e
           (log/error "Invalid id:" id)
           (log/error e)
           (-> (response {:message "id must be an integer value"})
               (status 422))))))

(def api
  (-> sms
      wrap-exception
      wrap-json-response
      wrap-keyword-params
      wrap-json-params
      wrap-params))
