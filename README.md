# medularis-chalensh

A solution for the Medularis coding test.

## Usage

API Usage:

### Running the tests

Having [leiningen](http://leiningen.org/#install) and some JDK installed:

```bash
lein test
```

### Deployment

Having installed and configured the
[Heroku toolbelt](https://toolbelt.heroku.com/) for your platform, perform:

```bash
heroku login
heroku addons:create heroku-postgresql:hobby-dev
heroku pg:psql < heroku.sql
heroku config:add TWILIO_SID={} AUTH_TOKEN={} TWILIO_FROM=+15005550006 TWILIO_TO={}
git push heroku master
```

### Invokation

Invoke using an HTTP client on the appropriate Heroku instance.
For the purposes of this test:

#### Sending a message

```bash
curl -H "Content-Type: application/json" -X POST -d '{"message":"Hello"}'  https://fathomless-refuge-18912.herokuapp.com/api/sms
```

#### Retrieving a message

```bash
curl https://fathomless-refuge-18912.herokuapp.com/api/sms/{id}
```

## License

Copyright © 2015 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
